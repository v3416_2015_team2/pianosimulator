#include "flogger.h"

#include <QDate>
#include <QTime>

void FLogger::print(const QString &message)
{
    _out << QTime::currentTime().toString("[hh:mm:ss] ").toStdString() << message.toStdString() << std::endl;
}

FLogger::FLogger()
{
    QString filename
            = QDate::currentDate().toString("yyyy-MM-dd")
            + QTime::currentTime().toString("_hh_mm_ss")
            + ".log";

    _out.open(filename.toStdString().c_str());
    _out << "Log successfully started!" << std::endl;
}

FLogger::~FLogger()
{
    _out << "End logging!" << std::endl;
    _out.close();
}

