QT       += core

TARGET = libflogger
TEMPLATE = lib
DEFINES = FLOGGER_LIB

SOURCES += \
    flogger.cpp

HEADERS  += \
    flogger.h

include(../common.pri)

DISTFILES += \
    libflogger_resource.rc
