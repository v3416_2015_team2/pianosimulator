#pragma once
#ifndef FLOGGER_H
#define FLOGGER_H

#if defined FLOGGER_LIB
#define FLOGGER_DLLSPEC Q_DECL_EXPORT
#else
#define FLOGGER_DLLSPEC Q_DECL_IMPORT
#endif

#include <fstream>
#include <QString>

class FLOGGER_DLLSPEC FLogger
{
public:
    static FLogger& instance()
    {
        static FLogger logger;
        return logger;
    }

    void print(QString const& message);

private:
    std::ofstream _out;

    FLogger();
    ~FLogger();
    FLogger(const FLogger&);
    FLogger& operator=(const FLogger&);
};



#endif // FLOGGER_H
