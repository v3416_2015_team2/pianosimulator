@echo off
echo Make sure tha all is allright
echo Would you really like to make a release now? (y/n)
set /P INPUT=Type input: %=%
If /I %INPUT% EQU y goto deploy 
goto failed

:deploy
git pull
if %errorlevel% NEQ 0 goto failed
:: Чтение текущей версии и инкремент сборки
set /p build=<build.txt
set /p version=<version.txt
echo Current version = %version%.%build%
set /a build=%build%+1
echo %build% > build.txt
echo VERSION = %version%.%build% > ..\version.pri
:: Проверить сборку
call build.bat
echo go next
if %errorlevel% NEQ 0 goto failed
:: Добавляем изменения в репозиторий
git add ..\pianosimulator\pianosimulator_resource.rc
if %errorlevel% NEQ 0 goto failed
git add ..\version.pri
if %errorlevel% NEQ 0 goto failed
git add build.txt
if %errorlevel% NEQ 0 goto failed
git add version.txt
if %errorlevel% NEQ 0 goto failed
git commit -m "Auto commit. New release %version%.%build%"
if %errorlevel% NEQ 0 goto failed
git tag %version%.%build%
if %errorlevel% NEQ 0 goto failed
git push origin master
if %errorlevel% NEQ 0 goto failed
git push origin --tags
if %errorlevel% NEQ 0 goto failed

echo Success!
exit 0
goto failed

:failed
echo Something goes wrong
exit 1
