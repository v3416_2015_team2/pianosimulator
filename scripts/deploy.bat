@echo off
set WINDEPLOYQT=windeployqt.exe
cd ..
mkdir deploy
%WINDEPLOYQT% --dir deploy bin\libflogger.dll
if %errorlevel% NEQ 0 goto failed
%WINDEPLOYQT% --dir deploy --qmldir pianosimulator\resource bin\pianosimulator.exe
if %errorlevel% NEQ 0 goto failed

exit /b 0

:failed
echo Something goes wrong
exit 1