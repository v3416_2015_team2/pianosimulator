@echo off
set QMAKE_PATH=qmake.exe
set MAKE_PATH=mingw32-make.exe -j 4
cd ..
rmdir bin /s /q
mkdir bin
cd flogger
%MAKE_PATH% clean
%QMAKE_PATH%
if %errorlevel% NEQ 0 goto failed
%MAKE_PATH%
if %errorlevel% NEQ 0 goto failed
cd ..\pianosimulator
%MAKE_PATH% clean
%QMAKE_PATH%
if %errorlevel% NEQ 0 goto failed
%MAKE_PATH%
if %errorlevel% NEQ 0 goto failed
cd ..\scripts
echo Compiling success!
exit /b 0

:failed
echo Something goes wrong
exit 1