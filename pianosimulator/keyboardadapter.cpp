#include "keyboardadapter.h"

#include "soundcontroller.h"

#include <QDebug>
#include <QQuickItem>


KeyboardAdapter::KeyboardAdapter(QWidget *parent)
    : QQuickWidget(parent)
{
    _isLearningMode = false;

    setSource(QUrl("qrc:/resource/qml/PianoKeyboard.qml"));
    setResizeMode(QQuickWidget::SizeRootObjectToView);

    connect(rootObject(), SIGNAL(pianoKeyPressed(int, int)), SoundController::getInstance(), SLOT(playSound(int,int)));
    connect(rootObject(), SIGNAL(pianoKeyReleased(int, int)), SoundController::getInstance(), SLOT(stopSound(int,int)));
    connect(rootObject(), SIGNAL(pianoKeyPressed(int, int)), SIGNAL(signalPianoKeyPressed(int,int)));
    connect(rootObject(), SIGNAL(pianoKeyReleased(int, int)), SIGNAL(signalPianoKeyReleased(int,int)));
}

void KeyboardAdapter::setDesiredKey(int octave, int key)
{
    if (_isLearningMode)
    {
        rootObject()->setProperty("desiredOctave", octave);
        rootObject()->setProperty("desiredKey", key);
    }
}

void KeyboardAdapter::endLearningMode()
{
    setLearningMode(false);
}

void KeyboardAdapter::setLearningMode(bool status)
{
    rootObject()->setProperty("isLearningMode", status);
    _isLearningMode = status;
}
