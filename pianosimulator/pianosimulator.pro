QT       += core gui quick multimedia quickwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pianosimulator
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp\
        mainwindow.cpp \
	keyboardadapter.cpp \
    soundcontroller.cpp \
    audioplayer.cpp \
    visualizer.cpp

HEADERS  += mainwindow.h \
        keyboardadapter.h \
    soundcontroller.h \
    audioplayer.h \
    visualizer.h

FORMS    += mainwindow.ui \
    visualizer.ui

RESOURCES += \
    resource.qrc

LIBS += -L$$PWD/../bin/ \
    -lflogger

INCLUDEPATH += $$PWD/../flogger

include(../common.pri)
include(../version.pri)

DEFINES += APP_VERSION=\\\"$$VERSION\\\"

DISTFILES += \
    pianosimulator_resource.rc
