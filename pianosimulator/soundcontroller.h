#ifndef SOUNDCONTROLLER_H
#define SOUNDCONTROLLER_H

#include <QMap>
#include <QObject>
#include <QSoundEffect>

class SoundController : public QObject
{
    Q_OBJECT
public:

    static SoundController * getInstance() {
        if (_instance == nullptr)
            _instance = new SoundController();

        return _instance;
    }

public slots:
    void playSound(int octave, int key);
    void stopSound(int octave, int key);

private slots:
    void finished();

private:
    explicit SoundController();
    virtual ~SoundController() {}

    QMap<int, QSoundEffect *> _soundMap;

    static SoundController * _instance;

    int getCode(int octave, int key);
};

#endif // SOUNDCONTROLLER_H
