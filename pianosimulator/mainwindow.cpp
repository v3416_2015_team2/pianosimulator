#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "flogger.h"

#include <QAction>
#include <QActionGroup>
#include <QApplication>
#include <QFileDialog>
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _pKeyboardAdapter = new KeyboardAdapter;
    _pVisualizer = new Visualizer;
    _pAudioPlayer = new AudioPlayer(this);

    connect(_pVisualizer, SIGNAL(startRecording()), _pAudioPlayer, SLOT(startRecording()));
    connect(_pVisualizer, SIGNAL(endRecording()), _pAudioPlayer, SLOT(endRecording()));
    connect(_pVisualizer, SIGNAL(playMelody()), _pAudioPlayer, SLOT(play()));

    connect(_pVisualizer, SIGNAL(onLearnClicked(bool)), _pKeyboardAdapter, SLOT(setLearningMode(bool)));
    connect(_pVisualizer, SIGNAL(onLearnClicked(bool)), _pAudioPlayer, SLOT(setLearningMode(bool)));
    connect(_pAudioPlayer, SIGNAL(nextDesiredKey(int,int)), _pKeyboardAdapter, SLOT(setDesiredKey(int,int)));

    connect(ui->actionDemo, SIGNAL(triggered()), _pAudioPlayer, SLOT(playDemo()));

    ui->verticalLayout->addWidget(_pVisualizer, 1);
    ui->verticalLayout->addWidget(_pKeyboardAdapter);


    connect(ui->actionAbout, SIGNAL(triggered()), SLOT(onActionAbout()));
    connect(ui->actionExit, SIGNAL(triggered()), SLOT(onActionExit()));
    connect(ui->actionOpen, SIGNAL(triggered()), SLOT(onActionOpen()));
    connect(ui->actionSave, SIGNAL(triggered()), SLOT(onActionSave()));

    connect(_pKeyboardAdapter, SIGNAL(signalPianoKeyPressed(int,int)), _pAudioPlayer, SLOT(addNote(int,int)));

    connect(_pAudioPlayer, SIGNAL(endLearningMode()), _pKeyboardAdapter, SLOT(endLearningMode()));
    connect(_pAudioPlayer, SIGNAL(endLearningMode()), _pVisualizer, SLOT(endLearningMode()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onActionAbout()
{
    FLogger::instance().print("MainWindow::onActionAbout();");

    std::ifstream input;
    input.open("");

    QMessageBox::about(this,
                       "About Program",
                       "Version " + QApplication::applicationVersion());
}

void MainWindow::onActionExit()
{
    FLogger::instance().print("MainWindow::onActionExit()");
    FLogger::instance().print("Application exit code = 0");

    QApplication::exit(0);
}

void MainWindow::onActionSave()
{
    FLogger::instance().print("MainWindow::onActionSave()");
    if (_pAudioPlayer->isEmpty())
    {
        QMessageBox::information(this, "Save", "Nothing to save!");
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save File"),
                                                    QDir::currentPath(),
                                                    tr("Music (*.psm)"));
    if (!fileName.isEmpty())
    {
        _pAudioPlayer->save(fileName);
    }
}

void MainWindow::onActionOpen()
{
    FLogger::instance().print("MainWindow::onActionOpen()");
    const QString fileName = QFileDialog::getOpenFileName(this,tr("Open File"), QDir::currentPath(), tr("Music (*.psm)"));
    if (!fileName.isEmpty())
    {
        _pAudioPlayer->load(fileName);
    }
}

