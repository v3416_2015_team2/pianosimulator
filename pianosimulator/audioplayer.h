#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H

#include <QObject>
#include <QTime>
#include <QVector>

class AudioPlayer : public QObject
{
    Q_OBJECT
public:
    explicit AudioPlayer(QObject *parent = 0);

    bool isEmpty();

public slots:
    void endRecording();
    void startRecording();
    void play();

    void save(QString filename);
    void load(QString filename);

    void playDemo();

    void addNote(int octave, int key);

    void setLearningMode(bool);

signals:
    void nextDesiredKey(int octave, int key);
    void endLearningMode();

private slots:
    void playNextSound();

private:
    struct Sound {
        int delay;
        int octave;
        int key;

        Sound() {}
        Sound(int _delay, int _octave, int _key) : delay(_delay) , octave(_octave), key(_key) {}
    };

    QVector<Sound> _song;
    int _pos;
    int _learnPos;
    bool _isLearning;
    bool isRecording;
    QTime _recordTime;
};

#endif // AUDIOPLAYER_H
