#include "visualizer.h"
#include "ui_visualizer.h"

Visualizer::Visualizer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Visualizer)
{
    ui->setupUi(this);

    connect(ui->pcmdPlay, SIGNAL(clicked()), SIGNAL(playMelody()));
    connect(ui->pcmdRecord, SIGNAL(clicked(bool)), SLOT(onRecordClicked(bool)));
    connect(ui->pcmdLearn, SIGNAL(clicked(bool)), SIGNAL(onLearnClicked(bool)));
}

Visualizer::~Visualizer()
{
    delete ui;
}

void Visualizer::endLearningMode()
{
    ui->pcmdLearn->setChecked(false);
}

void Visualizer::onRecordClicked(bool checked)
{
    if (checked)
        emit startRecording();
    else
        emit endRecording();
}
