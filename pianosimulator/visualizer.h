#ifndef VISUALIZER_H
#define VISUALIZER_H

#include <QWidget>

namespace Ui {
class Visualizer;
}

class Visualizer : public QWidget
{
    Q_OBJECT

public:
    explicit Visualizer(QWidget *parent = 0);
    ~Visualizer();

public slots:
    void endLearningMode();

signals:
    void endRecording();
    void startRecording();
    void playMelody();

    void onLearnClicked(bool);

private slots:
    void onRecordClicked(bool checked);

private:
    Ui::Visualizer *ui;
};

#endif // VISUALIZER_H
