#include "soundcontroller.h"

SoundController * SoundController::_instance = nullptr;

SoundController::SoundController()
{
    for (int octave = -2; octave <= 2; ++octave)
    {
        for (int key = 0; key <= 11; ++key)
        {
            _soundMap[getCode(octave, key)] = new QSoundEffect(this);
            _soundMap[getCode(octave, key)]->setSource(QUrl("qrc:/audio/sounds/" + QString::number(octave) + "_" + QString::number(key) + ".wav"));
        }
    }

}

int SoundController::getCode(int octave, int key)
{
    return octave * 12 + key + 24;
}

void SoundController::playSound(int octave, int key)
{
    Q_UNUSED(octave);
    if (_soundMap[getCode(octave, key)]) {
        _soundMap[getCode(octave, key)]->play();
    }
}

void SoundController::stopSound(int octave, int key)
{
    Q_UNUSED(octave);
    Q_UNUSED(key);
}

void SoundController::finished()
{
}

