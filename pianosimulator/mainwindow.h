#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "audioplayer.h"
#include "keyboardadapter.h"
#include "visualizer.h"

#include <QMainWindow>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

private slots:
    void onActionAbout();
    void onActionExit();
    void onActionOpen();
    void onActionSave();

private:
    AudioPlayer * _pAudioPlayer;
    KeyboardAdapter * _pKeyboardAdapter;
    Visualizer * _pVisualizer;

};

#endif // MAINWINDOW_H
