import QtQuick 2.3

Item
{
    id: root
    width: isWhite ? 46 : 36
    height: isWhite ? 290 : 170

    property bool isWhite: true

    property int blackShift: isWhite ? 4 : 8

    property string backgroundColor: isWhite ? "#BBBBBB" : "#444444"
    property string backgroundBorderColor: isWhite ? "#B0B0B0" : "transparent"
    property string keyColor: isWhite ? "#EEEEEE" : "#222222"

    property int keyCode: -1
    property bool desiredKey: false

    signal pianoKeyPressed(int code);
    signal pianoKeyReleased(int code);

    Rectangle
    {
        id: background
        y: -blackShift
        width: root.width
        height: root.height + blackShift
        color: backgroundColor
        border.color: backgroundBorderColor
        border.width: isWhite ? 1 : 0
        radius: isWhite ? 0 : 8
        smooth: true
    }

    Rectangle
    {
        id: key
        x: -1
        y: -blackShift
        width: parent.width - 2
        height: parent.height - 1 - 3 + blackShift
        color: keyColor
        radius: isWhite ? 4 : 8
        smooth: true

        Rectangle
        {
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            height: parent.height * 0.2
            visible: desiredKey & mainRoot.isLearningMode

            color: "green"
            opacity: 0.8
        }

        MouseArea
        {
            anchors.fill: parent

            onPressed: {
                key.x = 1;
                key.height = root.height - 1 + blackShift;

                if (isWhite)
                {
                    root.keyColor = "#DDDDDD"
                } else {
                    root.keyColor = "#333333"
                }

                root.pianoKeyPressed(keyCode);
            }

            onClicked:
            {
            }

            onReleased: {
                key.x = -1
                key.height = root.height - 1 - 3 + blackShift;

                if (isWhite)
                {
                    root.keyColor = "#EEEEEE"
                } else {
                    root.keyColor = "#222222"
                }

                root.pianoKeyReleased(keyCode);
            }
        }
    }

}
