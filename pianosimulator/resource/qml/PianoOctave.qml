import QtQuick 2.3

Item
{
    id: root

    // Настраиваемые параметры
    width: (161 * 2 + 8)
    height: 145 * 2
    property bool isMovable: false // Включить перемещие текущего элемента для компоновки

    property int octaveCode: -1

    scale: Math.min(width / item.width, height / item.height)

    signal pianoKeyPressed(int octaveCode, int keyCode);
    signal pianoKeyReleased(int octaveCode, int keyCode);

    // Собственно сам элемент
    Item
    {
        id: item
        // Логика элемента относительно этих размеров
        width: 161 * 2 + 8
        height: 145 * 2
        anchors.centerIn: root

        PianoKey
        {
           id: noteC
           x: 1
           keyCode: 0
           desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

           onPianoKeyPressed: {
               root.pianoKeyPressed(octaveCode, code);
           }

           onPianoKeyReleased: {
               root.pianoKeyReleased(octaveCode, code);
           }
        }

        PianoKey
        {
            id: noteD
            x: noteC.width + noteC.x + 1
            keyCode: 1
            desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

            onPianoKeyPressed: {
                root.pianoKeyPressed(octaveCode, code);
            }

            onPianoKeyReleased: {
                root.pianoKeyReleased(octaveCode, code);
            }
        }

        PianoKey
        {
            id: noteE
            x: noteC.width + noteD.x + 1
            keyCode: 2
            desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

            onPianoKeyPressed: {
                root.pianoKeyPressed(octaveCode, code);
            }

            onPianoKeyReleased: {
                root.pianoKeyReleased(octaveCode, code);
            }
        }

        PianoKey
        {
            id: noteF
            x: noteC.width + noteE.x + 1
            keyCode: 3
            desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

            onPianoKeyPressed: {
                root.pianoKeyPressed(octaveCode, code);
            }

            onPianoKeyReleased: {
                root.pianoKeyReleased(octaveCode, code);
            }
        }

        PianoKey
        {
            id: noteG
            x: noteC.width + noteF.x + 1
            keyCode: 4
            desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

            onPianoKeyPressed: {
                root.pianoKeyPressed(octaveCode, code);
            }

            onPianoKeyReleased: {
                root.pianoKeyReleased(octaveCode, code);
            }
        }

        PianoKey
        {
            id: noteA
            x: noteC.width + noteG.x + 1
            keyCode: 5
            desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

            onPianoKeyPressed: {
                root.pianoKeyPressed(octaveCode, code);
            }

            onPianoKeyReleased: {
                root.pianoKeyReleased(octaveCode, code);
            }
        }

        PianoKey
        {
            id: noteB
            x: noteC.width + noteA.x + 1
            keyCode: 6
            desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

            onPianoKeyPressed: {
                root.pianoKeyPressed(octaveCode, code);
            }

            onPianoKeyReleased: {
                root.pianoKeyReleased(octaveCode, code);
            }
        }

        PianoKey
        {
           id: noteDb
           x: noteD.x - width / 2 - 2
           isWhite: false
           keyCode: 7
           desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

           onPianoKeyPressed: {
               root.pianoKeyPressed(octaveCode, code);
           }

           onPianoKeyReleased: {
               root.pianoKeyReleased(octaveCode, code);
           }
        }
        PianoKey
        {
           id: noteEb
           x: noteE.x - width / 2 - 2
           isWhite: false
           keyCode: 8
           desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

           onPianoKeyPressed: {
               root.pianoKeyPressed(octaveCode, code);
           }

           onPianoKeyReleased: {
               root.pianoKeyReleased(octaveCode, code);
           }
        }

        PianoKey
        {
           id: noteGb
           x: noteG.x - width / 2 - 2
           isWhite: false
           keyCode: 9
           desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

           onPianoKeyPressed: {
               root.pianoKeyPressed(octaveCode, code);
           }

           onPianoKeyReleased: {
               root.pianoKeyReleased(octaveCode, code);
           }
        }

        PianoKey
        {
           id: noteAb
           x: noteA.x - width / 2 - 2
           isWhite: false
           keyCode: 10
           desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

           onPianoKeyPressed: {
               root.pianoKeyPressed(octaveCode, code);
           }

           onPianoKeyReleased: {
               root.pianoKeyReleased(octaveCode, code);
           }
        }

        PianoKey
        {
           id: noteBb
           x: noteB.x - width / 2 - 2
           isWhite: false
           keyCode: 11
           desiredKey: (octaveCode == mainRoot.desiredOctave) & (keyCode == mainRoot.desiredKey)

           onPianoKeyPressed: {
               root.pianoKeyPressed(octaveCode, code);
           }

           onPianoKeyReleased: {
               root.pianoKeyReleased(octaveCode, code);
           }
        }

    }
}
