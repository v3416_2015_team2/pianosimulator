import QtQuick 2.3
import QtQuick.Controls 1.4

Item
{
    id: mainRoot
    height: baseSizeH * mainRoot.width / item.width
    width: baseSizeW

    property bool isLearningMode: false
    property int desiredOctave: -2
    property int desiredKey: -2

    property int baseSizeH: 290
    property int baseSizeW: (161 * 2 + 8) * octaveNumber

    property int octaveNumber: 5

    signal pianoKeyPressed(int octave, int key);
    signal pianoKeyReleased(int octave, int key);
    transform: Scale {xScale: mainRoot.width / item.width; yScale: mainRoot.height / item.height; }

    Row {
        id: item
        height: baseSizeH
        width: baseSizeW
        spacing: 0

        Repeater
        {
            id: repeater
            model: mainRoot.octaveNumber
            PianoOctave
            {
                octaveCode: index - Math.floor(mainRoot.octaveNumber / 2)

                onPianoKeyPressed: {
                    mainRoot.pianoKeyPressed(octaveCode, keyCode);
                }

                onPianoKeyReleased: {
                    mainRoot.pianoKeyReleased(octaveCode, keyCode);
                }
            }

        }
    }
}






