#ifndef KEYBOARDADAPTER_H
#define KEYBOARDADAPTER_H

#include <QtQuickWidgets/QQuickWidget>

class KeyboardAdapter : public QQuickWidget
{
    Q_OBJECT
public:
    KeyboardAdapter(QWidget * parent = nullptr);

public slots:
    void setLearningMode(bool);
    void setDesiredKey(int octave, int key);

    void endLearningMode();

signals:
    void signalPianoKeyPressed(int octave, int key);
    void signalPianoKeyReleased(int octave, int key);

private:
    bool _isLearningMode;

};

#endif // KEYBOARDADAPTER_H
