#include "audioplayer.h"

#include "soundcontroller.h"

#include <QTimer>

#include <fstream>

#include "flogger.h"

#include <QMessageBox>

#include <QFileDialog>
#include <QTextStream>
#include <QDebug>


AudioPlayer::AudioPlayer(QObject *parent) : QObject(parent)
{
    FLogger::instance().print("AudioPlayer::AudioPlayer();");
    _pos = 0;
    _learnPos = 0;
    isRecording = false;
    _isLearning = false;
}

bool AudioPlayer::isEmpty()
{
    return _song.isEmpty();
}

void AudioPlayer::endRecording()
{
    FLogger::instance().print("AudioPlayer::endRecording();");
    isRecording = false;
}

void AudioPlayer::startRecording()
{
    FLogger::instance().print("AudioPlayer::startRecording();");
    _song.clear();
    isRecording = true;
    _recordTime.restart();
}

void AudioPlayer::play()
{
    FLogger::instance().print("AudioPlayer::play();");
    _pos = 0;
    for (int i = 0; i < _song.size(); ++i)
    {
        QTimer::singleShot(_song[i].delay, this, SLOT(playNextSound()));
    }
}

void AudioPlayer::save(QString filename)
{
    FLogger::instance().print("AudioPlayer::save(" + filename + ");");

    std::ofstream out(filename.toLocal8Bit().toStdString());

    out << _song.size() << std::endl;
    for (int i = 0; i < _song.size(); ++i)
        out << _song[i].delay << " " << _song[i].octave << " " << _song[i].key << std::endl;

    out.close();
}

void AudioPlayer::load(QString filename)
{
    FLogger::instance().print("AudioPlayer::load(" + filename + ");");
    _song.clear();

    std::ifstream in(filename.toLocal8Bit().toStdString());
    int n;
    in >> n;

    for (int i = 0; i < n; ++i)
    {
        int delay, octave, key;
        in >> delay >> octave >> key;
        _song.push_back(Sound(delay, octave, key));
    }

    in.close();
}

void AudioPlayer::playDemo()
{
    FLogger::instance().print("AudioPlayer::playDemo();");
    _song.clear();
    int delay = 0;
    for (int j = 0; j < 1; ++j)
        for (int i = 0; i <= 11; ++i)
            _song.push_back(Sound(delay += 250, 0, i));
    play();
}

void AudioPlayer::addNote(int octave, int key)
{
    if (isRecording)
        _song.push_back(Sound(_recordTime.elapsed(), octave, key));

    if (_isLearning && octave == _song[_learnPos].octave && key == _song[_learnPos].key)
    {
        _learnPos++;
        if (_learnPos >= _song.size())
            setLearningMode(false);
        else
            emit nextDesiredKey(_song[_learnPos].octave, _song[_learnPos].key);
    }
}

void AudioPlayer::setLearningMode(bool status)
{
    FLogger::instance().print("AudioPlayer::setLearningMode(bool); " + status);

    _learnPos = 0;
    _isLearning = status;

    if (status && _song.size()) {
        emit nextDesiredKey(_song[_learnPos].octave, _song[_learnPos].key);
    } else {
        _isLearning = false;
        emit nextDesiredKey(-2, -2);
        emit endLearningMode();
    }


}

void AudioPlayer::playNextSound()
{
    if (_pos >= _song.size())
        return;
    SoundController::getInstance()->playSound(_song[_pos].octave, _song[_pos].key);
    _pos++;
}

